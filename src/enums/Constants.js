'use strict';

const Enum = require('../core/base/Enum');

module.exports = new Enum({

    VALIDATION_ERROR: 'ValidationError',

})