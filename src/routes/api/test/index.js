'use strict';

const TestController = require('../../../controllers/SurveyController');

module.exports.autoroute = {
    get: {
        '/':TestController.actionIndex,
        '/:id':TestController.actionView
    },
    post: {
        '/file':TestController.actionCreate
    }
};