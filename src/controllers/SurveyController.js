'use strict';

const _isEmpty = require('lodash/isEmpty');

const config = require('./../config');
const Response = require('../core/rest/Response');
const {Survey, generateSearchQuery, sequelize} = require('./../data/models');

class SurveyController {
    static async actionIndex(req, res) {
        const {limit, page: currentPage, q = ''} = req.query;

        const query = !_isEmpty(q) ? generateSearchQuery(q, ['name']) : {};

        const {rows: surveys, count: total} = await Survey.findAndCountAll({
            where: {
                ...query
            },
            limit
        });

        return Response.ok(res, {
            surveys,
            _meta: {
                total,
                currentPage,
                pageCount: Math.ceil(total / limit)
            }
        });
    }

    static async actionView(req, res) {
        const id = req.params.id;

        const survey = await Survey.findOne({
            where: {id}
        });

        if (_isEmpty(survey)) {
            return Response.notFound(res, 'survey not found');
        }

        return Response.ok(res, {survey});
    }

    static async actionCreate(req, res) {
        const csv = require('csv-parser');
        const fs = require('fs');
        let csvData = [];

        await fs.createReadStream(__dirname + '/csv/survey.csv')
            .pipe(csv())
            .on('data', async (row) => {
                csvData.push(row)
            })
            .on('end', async () => {
                while (csvData.length !== 0) {
                    const arr = csvData.slice(0, Number(config.get('sliceOption')));
                    await sequelize.transaction(async (transaction) => {
                        await Survey.bulkCreate(arr, {transaction})
                    });
                    csvData.splice(0, Number(config.get('sliceOption')));
                }

                return Response.created(res, {})
            });
    }
}

module.exports = SurveyController;
