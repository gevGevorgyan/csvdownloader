'use strict';

module.exports = (sequelize, DataTypes) => {
    const Survey = sequelize.define(
        'Survey',
        {
            id: {
                primaryKey: true,
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4
            },
            Year: {
                type: DataTypes.STRING
            },
            Industry_aggregation_NZSIOC: {
                type: DataTypes.STRING
            },
            Industry_code_NZSIOC: {
                type: DataTypes.STRING
            },
            Units: {
                type: DataTypes.STRING
            },
            Variable_Name: {
                type: DataTypes.STRING
            },
            Variable_Category: {
                type: DataTypes.STRING
            },
            Value: {
                type: DataTypes.STRING
            },
            Industry_code_ANZSIC06: {
                type: DataTypes.STRING
            },
        },
        {
            tableName: 'survey',
            timestamps: true
        }
    );

    Survey.addScopes = () => {};

    return Survey;
};
