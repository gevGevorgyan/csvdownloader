'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('survey', {
            id: {
                primaryKey: true,
                type: Sequelize.UUID
            },
            Year: {
                type: Sequelize.STRING
            },
            Industry_aggregation_NZSIOC: {
                type: Sequelize.STRING
            },
            Industry_code_NZSIOC: {
                type: Sequelize.STRING
            },
            Units: {
                type: Sequelize.STRING
            },
            Variable_Name: {
                type: Sequelize.STRING
            },
            Variable_Category: {
                type: Sequelize.STRING
            },
            Value: {
                type: Sequelize.STRING
            },
            Industry_code_ANZSIC06: {
                type: Sequelize.STRING
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        })
    },

    async down(queryInterface) {
        await queryInterface.dropTable('survey')
    }
};