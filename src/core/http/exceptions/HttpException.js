'use strict';

const _ = require('underscore');
const _String = require('underscore.string');

const HttpStatus = require('../enums/HttpStatus');

export default class HttpException extends Error {
    constructor(httpStatus, message, errors) {
        super(message);

        let statusCode;
        let statusName;

        switch (typeof httpStatus) {
            case 'number':
                httpStatus = HttpStatus.get(httpStatus);
                if (!_.isUndefined(httpStatus)) {
                    statusCode = httpStatus.valueOf();
                    statusName = _String(httpStatus.key).humanize().value();
                }
                break;
            case 'object':
                statusCode = httpStatus.valueOf();
                statusName = _String(httpStatus.key).humanize().value();
                break;
            default:
                statusCode = httpStatus.INTERNAL_SERVER.valueOf();
                statusName = _String(httpStatus.key).humanize.value();
        }

        this.name = 'HttpException';
        this.statusCode = statusCode;
        this.statusName = statusName;
        this.message = message;
        this.errors = errors;
    }
}
