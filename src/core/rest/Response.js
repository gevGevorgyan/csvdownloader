'use strict';

const _ = require('underscore');
const _String = require('underscore.string');
const expressPaginate = require('express-paginate');

const HttpStatus = require('../http/enums/HttpStatus');
const Constants = require('../../enums/Constants');

class Response {

    static send(res, httpStatus, data = null, message = null) {
        return res.status(httpStatus.valueOf()).json(
            _.pick({
                status: httpStatus.valueOf(),
                name: _String(httpStatus.key).humanize().value(),
                data,
                message
            }, value => _.isNumber(value) || !_.isNull(value))
        )
    }

    static ok(res, data, message) {
        return this.send(res, HttpStatus.OK, data, message);
    }

    static created(res, data, message) {
        return this.send(res, HttpStatus.CREATED, data, message)
    }

    static unprocessableEntity(res, error, message) {
        if (error instanceof Error && error.hasOwnProperty('name') &&
            error.name === Constants.VALIDATION_ERROR.valueOf()) {
            let errors = [];

            try {
                if (error.hasOwnProperty('errors')) {
                    for (let field in error.errors) {
                        if (error.errors.hasOwnProperty(field)) {
                            let err = error.errors[field];
                            let message = err.kind === 'user defined' ? err.message : err.kind;

                            errors.push(_.pick({
                                field: err.path,
                                message: `err ${message.toLowerCase()}`,
                                alert: `err ${err.path}.${message.toLowerCase()}`,
                                value: _.isArray(err.value) ? null : err.value
                            }, value => !_.isEmpty(value)));
                        }
                    }
                }
            } catch (ex) {
                return this.send(res, HttpStatus.INTERNAL_SERVER, null, message);
            }

            return this.send(res, HttpStatus.UNPROCESSABLE_ENTITY, {
                error: _.isEmpty(errors) ? error : errors
            }, message || 'Validation failed');
        }

        return this.send(res, HttpStatus.INTERNAL_SERVER, null, message);
    }

    static badRequest(res, message) {
        return this.send(res, HttpStatus.BAD_REQUEST, null, message);
    }

    static forbidden(res, message) {
        return this.send(res, HttpStatus.FORBIDDEN, null, message)
    }

    static notFound(res, message) {
        return this.send(res, HttpStatus.NOT_FOUND, null, message)
    }

    static internalServer(res, message) {
        return this.send(res, HttpStatus.INTERNAL_SERVER, null, message);
    }

    static pageable(req, res, docs) {

        let _meta = {
            hasMore: expressPaginate.hasNextPages(req)(docs.pages),
            totalCount: docs.total,
            currentPage: req.query.page,
            pageCount: docs.pages,
            perPage: docs.limit
        };

        res.setHeader('X-Pagination-Total-count', _meta.totalCount || 0);
        res.setHeader('X-Pagination-Current-Page', _meta.currentPage || 0);
        res.setHeader('X-Pagination-Page-Count', _meta.pageCount || 0);
        res.setHeader('X-Pagination-Per-Page', _meta.perPage || 0);

        return _meta;
    }
}

module.exports = Response;
