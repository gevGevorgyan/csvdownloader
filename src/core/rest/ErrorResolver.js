'use strict';

const _ = require('underscore');

const HttpStatus = require('./../http/enums/HttpStatus');

module.exports = (err, req, res, next) => {
    let statusCode = err.statusCode || HttpStatus.INTERNAL_SERVER.valueOf();
    let statusName = err.statusName || HttpStatus.INTERNAL_SERVER.key;

    let response = {
        code: statusCode,
        name: statusName,
        message: err.name === 'HttpException' ? err.message : null
    };

    return res.status(statusCode).json(_.pick(response, val => _.isNumber(val) || !_.isEmpty(val)));
};