const BaseEnum =require('enum');

class Enum extends BaseEnum {

    valueOf() {
        let values = [];

        this.enums.map((item) => {
            values.push(item.value);
        });

        return values;
    }
}

module.exports = Enum;