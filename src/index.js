'use strict';

const http = require('http');
const path = require( 'path');
const express = require( 'express');
const bodyParser = require( 'body-parser');
const fileUpload = require("express-fileupload");
const autoRouter = require( 'express-autoroute');
const expressPaginate = require( 'express-paginate');

const NotFoundHttpException = require( "./core/http/exceptions/NotFoundHttpException");
const errorResolver = require( './core/rest/ErrorResolver');
const config = require( './config/index');

const app = express();
const server = http.createServer(app);

let port = config.get('server:port');
server.listen(port);

app.use(fileUpload());
app.use(bodyParser.json());
app.use(express.static('public'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(expressPaginate.middleware(config.get('response:limit:default'), config.get('response:limit:max'), config.get('response:offset')));

autoRouter(app, {
    routesDir: path.join(__dirname, 'routes')
});

app.use((req, res, next) => {
    next(new NotFoundHttpException());
});

app.use(errorResolver);

server.on('error', (error) => {
    if (error.syscall !== 'listen') {
        throw error;
    }

    let bind = typeof port === 'string' ? `pipe ${port}` : `port ${port}`;

    switch (error.code) {
        case 'EADDRINUSE':
            console.log(`${bind} is already in use`);
            process.exit(1);
            break;
        case 'EACCES':
            console.log(`${bind} requires elevated privileges`);
            process.exit(1);
            break;
        default:
            throw error;
    }
});

server.on('listening', () => {
    let address = server.address();
    let bind = typeof address === 'string' ? `pipe ${address}` : `port ${address.port}`;
    console.log(`Listening on ${bind}`);
});

module.exports = server;
